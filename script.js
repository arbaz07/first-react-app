function ActiveFriendsList(props) {
  return (
    <ul>
     {props.list.map((name)=>(
        <li key={name}>
          <span>{name}</span>
          <button onClick ={()=>props.onRemoveFriend(name)}>Remove</button>
          <button onClick ={()=>props.deactivateFriend(name)}>Deactivate</button>
        </li>

      ))}   
    </ul>
  )
}
function InactiveFriends(props) {
  return (
    <ul>
     {props.list.map((name)=>(
        <li key={name}>
          <span>{name}</span>
          <button onClick ={()=>props.activeFriends(name)}>Activate</button>
        </li>

      ))}   
    </ul>
  )
}


class App extends React.Component{

  constructor(props){
    super(props)

    this.state = {
      friends:["Arbaz", "Salman", "Khan"],
      input:'',
      inactiveFriends:[],

    }

    this.handleRemoveFriend=this.handleRemoveFriend.bind(this);
    this.updateInput = this.updateInput.bind(this);
    this.handleAddFriend = this.handleAddFriend.bind(this)
    this.moveTOInactive = this.moveTOInactive.bind(this)
    this.moveToActive = this.moveToActive.bind(this)
    this.clearAllFriends = this.clearAllFriends.bind(this)
  }

  handleAddFriend(){
    this.setState((currentState)=>{
      if(this.state.input==''){
        return;
      }
      return {
        friends : currentState.friends.concat([currentState.input]),
        input:""
      }
    })
  }

  handleRemoveFriend(name){
    this.setState((currentState)=>{
      return {
        friends:currentState.friends.filter((friend)=> friend!== name)
      }
    })
  }

  updateInput(e){
    const value = e.target.value;

    this.setState({
      input:value 
    })
  }

  clearAllFriends(){
    this.setState(()=>{
      return {
        friends:[],
        inactiveFriends:[]
      }
    })
  }

  moveToActive(name){
    this.setState((currentState)=>{
      return{
        friends:currentState.friends.concat([name]),
        inactiveFriends:currentState.inactiveFriends.filter((friend)=>friend!==name)

      }
    })
  }

  moveTOInactive(name){
    // handleRemoveFriend(name);
    this.setState((currentState)=>{
      return{
        friends:currentState.friends.filter((friend)=> friend!== name),
        inactiveFriends:currentState.inactiveFriends.concat([name])
      }
    })
    // console.log('name :>> ', name);
  }

  render(){  
    return (
      <div>
        <input
          type="text"
          placeholder = "new Friend"
          value={this.state.input}
          onChange={this.updateInput}
        />
        <button onClick={this.handleAddFriend}>Submit</button>
        <br/>
        <button onClick={this.clearAllFriends}>ClearAll</button>   
        <h1>Active Friends</h1>  

        <ActiveFriendsList 
          list={this.state.friends}
          onRemoveFriend = {this.handleRemoveFriend}
          deactivateFriend = {this.moveTOInactive}
        />

        <h1>Inactive Friends</h1>

        <InactiveFriends
          list={this.state.inactiveFriends}
          activeFriends = {this.moveToActive}
        />
      </div>
      )
  }
}






ReactDOM.render(<App />, document.getElementById("app"));
